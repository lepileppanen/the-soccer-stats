# README #

The Soccer Stats is a Wordpress plugin for your football (soccer) team.

### How do I get set up? ###

* Just git clone in your plugins-directory, activate the plugin and you're ready to go :)
* Homepage: http://thesoccerstats.wordpress.com

### Bugs? ###

* Use issues to report bugs